﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AgrizonAPI.Models;

namespace AgrizonAPI.Controllers
{
    public class CompanyProfileController : ApiController
    {
        // GET: CompanyProfile
        public IEnumerable<CompanyProfile> Get()
        {
            string query = "select * from tbl.CompanyProfile";

            DataTable dt = Database.get_DataTable(query);
            List<CompanyProfile> profile = new List<Models.CompanyProfile>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadCompanyProfile(dr));
                }
            }
            return profile;
        }

        public IEnumerable<CompanyProfile> Get(int id)
        {
            string query = "select * from tbl.CompanyProfile where companyProfileId='" + id + "'";
            DataTable dt = Database.get_DataTable(query);
            List<CompanyProfile> profile = new List<Models.CompanyProfile>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadCompanyProfile(dr));
                }
            }
            return profile;
        }

        public string Post([FromBody]CreateCompanyProfile values)
        {
            try
            {
                string query_companyprofile = "insert into tbl.CompanyProfile(dtmAdd,dtmUpdate,isValid,isStatus,profileId,userId,companyName,companyAddress,companyEmail,companyMobile,companyType" +
                    ",companyGST,companybankName,companybankAccount,companybankIfsc,companybankBranch) values ('" + DateTime.Now.ToString()
                        + "','" + DateTime.Now.ToString() + "','1','1','" + values.profileId + "','" + values.userId + "','" + values.companyName + "','" + values.companyAddress + "','" + values.companyEmail
                        + "','" + values.companyMobile + "','" + values.companyType + "','" + values.companyGST + "','" + values.companybankName + "','" + values.companybankAccount
                        + "','" + values.companybankIfsc + "','" + values.companybankBranch + "')";
                int res = Database.Execute(query_companyprofile);
                if (res == 1)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }

        }

        public string Put(int id, [FromBody]CreateCompanyProfile value)
        {
            string query = "update tbl.CompanyProfile set dtmUpdate='" + value.dtmUpdate + "', isValid='" + value.isValid + "',isStatus='" + value.isStatus
                + "',companyName='" + value.companyName + "',companyAddress='" + value.companyAddress + "',companyEmail='" + value.companyEmail
                + "',companyMobile='" + value.companyMobile + "',companyType='" + value.companyType + "',companyGST='" + value.companyGST + "',companybankName='" + value.companybankName
                + "',companybankAccount='" + value.companybankAccount + "',companybankIfsc='" + value.companybankIfsc + "',companybankBranch='" + value.companybankBranch
                + "' where companyProfileId='" + id + "'";
            int res = Database.Execute(query);
            if (res == 1)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }    
}