﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AgrizonAPI.Models;


namespace AgrizonAPI.Controllers
{
    public class AlertController : ApiController
    {
        // GET: Alert
        public IEnumerable<Alert> Get()
        {
            string query = "select * from tbl.Alert";

            DataTable dt = Database.get_DataTable(query);
            List<Alert> alert = new List<Models.Alert>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    alert.Add(new ReadAlert(dr));
                }
            }
            return alert;
        }

        public IEnumerable<Alert> Get(int id)
        {
            string query = "select * from tbl.Alert where userid='" + id + "''";

            DataTable dt = Database.get_DataTable(query);
            List<Alert> alert = new List<Models.Alert>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    alert.Add(new ReadAlert(dr));
                }
            }
            return alert;
        }

        public IEnumerable<Alert> GetAlert(int userId,int landId,int cropId)
        {
            string query = "";
            //if (alertId != 0)
            //{
            //    query = "select * from tbl.Alert where alertId='" + alertId + "'";
            //}
            //else if(userId != 0)
            //{
            //    query = "select * from tbl.Alert where userid='" + userId + "'";
            //}
            //else if(landId != 0)
            //{
            //    query = "select * from tbl.Alert where landId='" + landId + "'";
            //}
            //else if (cropId != 0)
            //{
            //    query = "select * from tbl.Alert where cropId='" + cropId + "'";
            //}

            if (userId != 0)
            {
                query = "select * from tbl.Alert where userid='" + userId + "'";

                if (landId != 0)
                {
                    query = "select * from tbl.Alert where userid='" + userId + "' and landId='" + landId + "'";

                    if (cropId != 0)
                    {
                        query = "select * from tbl.Alert where userid='" + userId + "'and landId='" + landId + "' and cropId='" + cropId + "'";
                    }
                }
            }

            DataTable dt = Database.get_DataTable(query);
            List<Alert> alert = new List<Models.Alert>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    alert.Add(new ReadAlert(dr));
                }
            }
            return alert;
        }

        public string Post([FromBody]CreateAlert values)
        {
            try
            {
                string sqlQuery = "", sqlQueryVal = "";
                if (values.landId != null)
                {
                    sqlQuery += "landId,";
                    sqlQueryVal += "'" + values.landId + "',";
                }

                if (values.cropId != null)
                {
                    sqlQuery += "cropId,";
                    sqlQueryVal += "'" + values.cropId + "',";
                }

                if (values.alertSource != null)
                {
                    sqlQuery += "alertSource,";
                    sqlQueryVal += "'" + values.alertSource + "',";
                }

                if (values.alertMessage != null)
                {
                    sqlQuery += "alertMessage,";
                    sqlQueryVal += "'" + values.alertMessage + "',";
                }

                if (values.alertRead != null)
                {
                    sqlQuery += "alertRead,";
                    sqlQueryVal += "'" + values.alertRead + "',";
                }

                if (values.alertLevel != null)
                {
                    sqlQuery += "alertLevel,";
                    sqlQueryVal += "'" + values.alertLevel + "',";
                }

                if (values.alertDomain != null)
                {
                    sqlQuery += "alertDomain,";
                    sqlQueryVal += "'" + values.alertDomain + "',";
                }

                if (values.alertModule != null)
                {
                    sqlQuery += "alertModule,";
                    sqlQueryVal += "'" + values.alertModule + "',";
                }

                string query = "insert into tbl.Alert(dtmAdd,dtmUpdate," + sqlQuery + " userId) values ('" + DateTime.Now.ToString()
                        + "','" + DateTime.Now.ToString() + "'," + sqlQueryVal + " '" + values.userId + "')";
                int res = Database.Execute(query);
                if (res == 1)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }

        public string Put(int id, [FromBody]CreateAlert value)
        {
            try
            {
                string sqlQuery = "";
                if (value.isValid != null)
                {
                    sqlQuery += "isValid='" + value.isValid + "',";
                }
                if (value.landId != null)
                {
                    sqlQuery += "landId='" + value.landId + "',";
                }
                if (value.cropId != null)
                {
                    sqlQuery += "cropId='" + value.cropId + "',";
                }
                if (value.cropId != null)
                {
                    sqlQuery += "cropId='" + value.cropId + "',";
                }
                if (value.alertSource != null)
                {
                    sqlQuery += "alertSource='" + value.alertSource + "',";
                }
                if (value.alertMessage != null)
                {
                    sqlQuery += "alertMessage='" + value.alertMessage + "',";
                }
                if (value.alertRead != null)
                {
                    sqlQuery += "alertRead='" + value.alertRead + "',";
                }
                if (value.alertLevel != null)
                {
                    sqlQuery += "alertLevel='" + value.alertLevel + "',";
                }
                if (value.alertDomain != null)
                {
                    sqlQuery += "alertDomain='" + value.alertDomain + "',";
                }
                if (value.alertDomain != null)
                {
                    sqlQuery += "alertDomain='" + value.alertDomain + "',";
                }
                if (value.alertModule != null)
                {
                    sqlQuery += "alertModule='" + value.alertModule + "',";
                }
                string query = "update tbl.Alert set " + sqlQuery + " dtmUpdate='" + value.dtmUpdate + "', isStatus='" + value.isStatus
                    + "' where alertId='" + id + "'";
                int res = Database.Execute(query);
                if (res == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
            catch(Exception ex)
            {
                return "-1";
            }
        }
    }
}