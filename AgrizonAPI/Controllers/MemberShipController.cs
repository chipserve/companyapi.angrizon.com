﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AgrizonAPI.Models;

namespace AgrizonAPI.Controllers
{
    public class MemberShipController : ApiController
    {
        // GET: MemberShip

        public IEnumerable<MemberShip> Get()
        {
            string query = "select * from tbl.MemberShip";

            DataTable dt = Database.get_DataTable(query);
            List<MemberShip> profile = new List<Models.MemberShip>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadMemberShip(dr));
                }
            }
            return profile;
        }

        public IEnumerable<MemberShip> Get(int id)
        {
            string query = "select * from tbl.MemberShip where userid='" + id + "'";
            DataTable dt = Database.get_DataTable(query);
            List<MemberShip> profile = new List<Models.MemberShip>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadMemberShip(dr));
                }
            }
            return profile;
        }

        public string Post([FromBody]CreateMemberShip values)
        {
            try
            {
                string sqlQuery = "", sqlQueryVal = "";

                if (values.profileId != null)
                {
                    sqlQuery += "profileId,";
                    sqlQueryVal += "'" + values.profileId + "',";
                }

                if (values.userId != null)
                {
                    sqlQuery += "userId,";
                    sqlQueryVal += "'" + values.userId + "',";
                }

                if (values.companyProfileId != null)
                {
                    sqlQuery += "companyProfileId,";
                    sqlQueryVal += "'" + values.companyProfileId + "',";
                }

                if (values.name != null)
                {
                    sqlQuery += "name,";
                    sqlQueryVal += "'" + values.name + "',";
                }

                if (values.emailId != null)
                {
                    sqlQuery += "emailId,";
                    sqlQueryVal += "'" + values.emailId + "',";
                }

                if (values.mobile != null)
                {
                    sqlQuery += "mobile,";
                    sqlQueryVal += "'" + values.mobile + "',";
                }

                if (values.dob != null)
                {
                    sqlQuery += "dob,";
                    sqlQueryVal += "'" + values.dob + "',";
                }

                if (values.address != null)
                {
                    sqlQuery += "address,";
                    sqlQueryVal += "'" + values.address + "',";
                }

                if (values.state != null)
                {
                    sqlQuery += "state,";
                    sqlQueryVal += "'" + values.state + "',";
                }

                if (values.city != null)
                {
                    sqlQuery += "city,";
                    sqlQueryVal += "'" + values.city + "',";
                }

                if (values.pincode != null)
                {
                    sqlQuery += "pincode,";
                    sqlQueryVal += "'" + values.pincode + "',";
                }

                if (values.aadhaarNo != null)
                {
                    sqlQuery += "aadhaarNo,";
                    sqlQueryVal += "'" + values.aadhaarNo + "',";
                }

                if (values.bankName != null)
                {
                    sqlQuery += "bankName,";
                    sqlQueryVal += "'" + values.bankName + "',";
                }

                if (values.bankAccount != null)
                {
                    sqlQuery += "bankAccount,";
                    sqlQueryVal += "'" + values.bankAccount + "',";
                }

                if (values.bankIfsc != null)
                {
                    sqlQuery += "bankIfsc,";
                    sqlQueryVal += "'" + values.bankIfsc + "',";
                }

                if (values.bankBranch != null)
                {
                    sqlQuery += "bankBranch,";
                    sqlQueryVal += "'" + values.bankBranch + "',";
                }

                string query_membership = "insert into tbl.MemberShip(dtmAdd,dtmUpdate,isValid," + sqlQuery + "isStatus) values ('" + DateTime.Now.ToString()
                        + "','" + DateTime.Now.ToString() + "','1'," + sqlQueryVal + "'1')";
                int res = Database.Execute(query_membership);
                string memId = "";
                if (res == 1)
                {
                    string query = "select top 1 memberId from tbl.MemberShip order by memberId desc";
                    DataSet ds = Database.get_DataSet(query);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        memId= ds.Tables[0].Rows[0]["memberId"].ToString();
                       
                    }
                    return "1-" + memId.ToString();
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }
      
        public string Put(int id,int memberid, [FromBody]CreateMemberShip value)
        {
            string sqlQuery = "";
            if(value.name!=null)
            {
                sqlQuery += "name='" + value.name + "',";
            }

            if (value.emailId != null)
            {
                sqlQuery += "emailId='" + value.emailId + "',";
            }

            if (value.mobile != null)
            {
                sqlQuery += "mobile='" + value.mobile + "',";
            }


            if (value.pincode != null)
            {
                sqlQuery += "pincode='" + value.pincode + "',";
            }

            if (value.aadhaarNo != null)
            {
                sqlQuery += "aadhaarNo='" + value.aadhaarNo + "',";
            }

            if (value.bankName != null)
            {
                sqlQuery += "bankName='" + value.bankName + "',";
            }

            if (value.bankAccount != null)
            {
                sqlQuery += "bankAccount='" + value.bankAccount + "',";
            }

            if (value.bankIfsc != null)
            {
                sqlQuery += "bankIfsc='" + value.bankIfsc + "',";
            }

            if (value.bankBranch != null)
            {
                sqlQuery += "bankBranch='" + value.bankBranch + "',";
            }

            if (value.address != null)
            {
                sqlQuery += "address='" + value.address + "',";
            }

            if (value.state != null)
            {
                sqlQuery += "state='" + value.state + "',";
            }

            if (value.city != null)
            {
                sqlQuery += "city='" + value.city + "',";
            }

            string query = "update tbl.MemberShip set dtmUpdate='" + DateTime.Now.ToString() + "', isValid='" + value.isValid
                + "'," + sqlQuery + " isStatus='" + value.isStatus + "' where memberid='" + memberid + "'";
            string memId = "";
            int res = Database.Execute(query);
            if (res == 1)
            {
                query = "select top 1 memberId from tbl.MemberShip where memberid='" + memberid + "' order by memberId desc";
                DataSet ds = Database.get_DataSet(query);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    memId = ds.Tables[0].Rows[0]["memberId"].ToString();

                }
                return "1-" + memId.ToString();
            }
            else
            {
                return "0";
            }
        }
    }
}