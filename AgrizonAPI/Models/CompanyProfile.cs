﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgrizonAPI.Models
{
    public class CompanyProfile
    {
        public int companyProfileId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }       
        public string  isStatus { get; set; }
        public int profileId { get; set; }
        public int userId { get; set; }
        public string companyName { get; set; }
        public string companyAddress { get; set; }
        public string companyEmail { get; set; }
        public string companyMobile { get; set; }
        public string companyType { get; set; }
        public string companyGST { get; set; }
        public string companybankName { get; set; }
        public string companybankAccount { get; set; }
        public string companybankIfsc { get; set; }
        public string companybankBranch { get; set; }

    }

    public class CreateCompanyProfile : CompanyProfile
    {

    }

    public class ReadCompanyProfile:CompanyProfile
    {
        public ReadCompanyProfile(DataRow dr)
        {
            companyProfileId = Convert.ToInt32(dr["companyProfileId"]);
            dtmAdd = dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            isStatus = dr["isStatus"].ToString();
            profileId = Convert.ToInt32(dr["profileId"]);
            userId = Convert.ToInt32(dr["userId"]);
            companyName = dr["companyName"].ToString();
            companyAddress = dr["companyAddress"].ToString();
            companyEmail = dr["companyEmail"].ToString();
            companyMobile = dr["companyMobile"].ToString();
            companyType = dr["companyType"].ToString();
            companyGST = dr["companyGST"].ToString();
            companybankName = dr["companybankName"].ToString();
            companybankAccount = dr["companybankAccount"].ToString();
            companybankIfsc = dr["companybankIfsc"].ToString();
            companybankBranch = dr["companybankBranch"].ToString();
        }
    }
}