﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgrizonAPI.Models
{
    public class Alert
    {
        public int alertId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }
        public string isStatus { get; set; }
        public string userId { get; set; }
        public string landId { get; set; }
        public string cropId { get; set; }
        public string alertSource { get; set; }
        public string alertMessage { get; set; }
        public string alertRead { get; set; }
        public string alertLevel { get; set; }
        public string alertDomain { get; set; }
        public string alertModule { get; set; }
    }

    public class CreateAlert: Alert
    {

    }

    public class ReadAlert: Alert
    {
        public ReadAlert(DataRow dr)
        {
            alertId = Convert.ToInt32(dr["alertId"].ToString());
            dtmAdd =dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            isStatus = dr["isStatus"].ToString();
            userId = dr["userId"].ToString();
            landId = dr["landId"].ToString();
            cropId = dr["cropId"].ToString();
            alertSource = dr["alertSource"].ToString();
            alertMessage = dr["alertMessage"].ToString();
            alertRead = dr["alertRead"].ToString();
            alertLevel = dr["alertLevel"].ToString();
            alertDomain = dr["alertDomain"].ToString();
            alertModule = dr["alertModule"].ToString();
        }
    }
}