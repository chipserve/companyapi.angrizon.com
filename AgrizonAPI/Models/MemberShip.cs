﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgrizonAPI.Models
{
    public class MemberShip
    {
        public int memberId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }
        public string isStatus { get; set; }
        public int profileId { get; set; }
        public int userId { get; set; }
        public int companyProfileId { get; set; }
        public string name { get; set; }
        public string emailId { get; set; }
        public string mobile { get; set; }
        public string dob { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string pincode { get; set; }
        public string aadhaarNo { get; set; }
        public string bankName { get; set; }
        public string bankAccount { get; set; }
        public string bankIfsc { get; set; }
        public string bankBranch { get; set; }
    }

    public class CreateMemberShip:MemberShip
    {

    }

    public class ReadMemberShip:MemberShip
    {
        public ReadMemberShip(DataRow dr)
        {
            memberId = Convert.ToInt32(dr["memberId"]);
            dtmAdd = dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            isStatus = dr["isStatus"].ToString();
            profileId = Convert.ToInt32(dr["profileId"]);
            userId = Convert.ToInt32(dr["userId"]);
            companyProfileId = Convert.ToInt32(dr["companyProfileId"]);
            name = dr["name"].ToString();
            emailId = dr["emailId"].ToString();
            mobile = dr["mobile"].ToString();
            dob = dr["dob"].ToString();
            address = dr["address"].ToString();
            state = dr["state"].ToString();
            city = dr["city"].ToString();
            pincode = dr["pincode"].ToString();
            aadhaarNo = dr["aadhaarNo"].ToString();
            bankName = dr["bankName"].ToString();
            bankAccount = dr["bankAccount"].ToString();
            bankIfsc = dr["bankIfsc"].ToString();
            bankBranch = dr["bankBranch"].ToString();
        }
    }
}